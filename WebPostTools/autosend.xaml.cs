﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WebPostTools
{
    /// <summary>
    /// autosend.xaml 的交互逻辑
    /// </summary>
    public partial class autosend : Window
    {
        DataTable dt = new DataTable();
        System.Windows.Media.Effects.DropShadowEffect errorEffect = new System.Windows.Media.Effects.DropShadowEffect();
        int willSendCount = -1;
        public autosend()
        {
            InitializeComponent();

            dt.Columns.Add("attribute");
            dt.Columns.Add("value");
            data.DataContext = dt;

            errorEffect.BlurRadius = 5;
            errorEffect.Color = Colors.Red;
            errorEffect.Direction = 0;
            errorEffect.ShadowDepth = 0;
        }

        private void insert_Click(object sender, RoutedEventArgs e)
        {
            TableAddItem();
        }

        private void startsend_Click(object sender, RoutedEventArgs e)
        {
            //开始发送
            string[] willsend = new string[willSendCount];

            for (int k = 0; k < dt.Rows.Count; k++)//列
            {
                //属性名
                string atr = dt.Rows[k].ItemArray[0].ToString();

                //分解
                string[] values = dt.Rows[k].ItemArray[1].ToString().Split('|');
                //组装
                for (int i = 0; i < values.Length; i++)
                {
                    willsend[i] += (k == 0 ? "" : "&") + atr + "=" + values[i];
                }
            }

            MainWindow.Inst.StartAutoSend(TB_url.Text,willsend);
           
        }
        private void add_Click(object sender, RoutedEventArgs e)
        {
            TableAddItem();
        }
        private void TableAddItem()
        {
            //校验空值
            if (string.IsNullOrEmpty(TB_attribute.Text))
            {
                TB_attribute.Effect = errorEffect;
                TB_attribute.ToolTip = "值为空";
                return;
            }
            if (string.IsNullOrEmpty(TB_value.Text))
            {
                TB_value.Effect = errorEffect;
                TB_value.ToolTip = "值为空";
                return;
            }
            //校验值的个数
            string[] values = TB_value.Text.Split('|');
            if (willSendCount != -1)
            {
                //校验值 是不是与第一次加的个数相同
                if (willSendCount != values.Length)
                {
                    TB_value.Effect = errorEffect;
                    TB_value.ToolTip = "与之前加的个数不一样";
                    return;
                }
            }
            else
            {
                //第一次加 得出值.
                willSendCount = values.Length;
            }

            dt.Rows.Add(new object[] { TB_attribute.Text, TB_value.Text });
            TB_attribute.Text = null;
            TB_value.Text = null;



        }

        private void TB_attribute_GotFocus(object sender, RoutedEventArgs e)
        {
            TB_attribute.Effect = null;
            TB_attribute.ToolTip = null;
        }

        private void TB_value_GotFocus(object sender, RoutedEventArgs e)
        {
            TB_value.Effect = null;
            TB_value.ToolTip = null;
        }
    }
}
namespace TextboxOrder
{
    class TextboxOrder
    {
        private string eventName;
        private string customerReference;
        public string EventName
        {
            get { return eventName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ApplicationException("null string");
                }
                else
                {
                    this.eventName = value;
                }
            }
        }
        public string CustomerReference
        {
            get { return customerReference; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ApplicationException("null string");
                }
                else
                {
                    this.customerReference = value;
                }
            }
        }
    }
}
