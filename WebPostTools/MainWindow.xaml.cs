﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DotNet.Utilities;

namespace WebPostTools
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public static MainWindow Inst;
        public MainWindow()
        {
            InitializeComponent();
            Inst = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string url = TB_url.Text;
            TB_result.Text = null;
            string postdata = TB_postdata.Text;

            if ((bool)RB_get.IsChecked)
            {
                //get
                TB_result.Text = HttpHelper.HttpGetHtml(url);
            }
            else
            {
                //post
                TB_result.Text = HttpHelper.HttpPost(url, postdata);
            }
        }

        public void StartAutoSend(string url,string[] postdatas)
        {
            TB_result.Text = null;
            TB_url.Text = url;
            foreach (string postdata in postdatas)
            {
                //显示当前发送
                TB_postdata.Text = postdata;
                //发送
                string r = HttpHelper.HttpPost(url, postdata);
                //显示结果
                TB_result.Text += postdata + "的结果\r\n-------------------------\r\n" + r + "\r\n";
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            autosend s = new autosend();
            s.Show();
        }
    }
}
